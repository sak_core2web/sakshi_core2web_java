import java.io.*;
class p6{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows: " );
		int row=Integer.parseInt(br.readLine());
		int ch='A';
		for(int i=1;i<=row;i++){
			int num=1;
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(num+ " ");
					num++;
				}
				else{
					System.out.print((char)ch+ " ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}
