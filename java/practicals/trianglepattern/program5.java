import java.io.*;
class p5{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows: " );
		int row=Integer.parseInt(br.readLine());
		int ch=row+65;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print((char)ch+" ");
				ch++;
			}
			System.out.println();
		}
	}
}
