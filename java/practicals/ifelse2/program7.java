class pr7{
	public static void main(String [] args){
		float costprice=120f;
		float sellingprice=100f;
		float difference= sellingprice-costprice;
		if(difference>0){
			float profit=difference;
			System.out.println("profit of "+profit);
		}
		else if(difference<0){
			float loss=-difference;
			System.out.println("loss of "+loss);
		}
		else{
			System.out.println("no profit no loss");
		}
	}
}
