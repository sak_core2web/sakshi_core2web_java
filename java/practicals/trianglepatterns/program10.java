import java.util.*;
class p10{
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("rows: ");
		int row=sc.nextInt();
		int magic=row%2;
		for(int i=1;i<=row;i++){
			int ch=i+64;
			for(int j=1;j<=row+1-i;j++){
				if(ch%2==magic){
					System.out.print((char)ch++ +" ");
				}
				else{
					System.out.print(ch++ +" ");
				}
			}
			System.out.println();
		}

	}
}
