class Demo{
	public static void main(String [] args){
		int a=10;
		int b=2;
		System.out.println(a+b);//12
		System.out.println(a-b);//8
		System.out.println(a*b);//20
		System.out.println(a/b);//5
		System.out.println(a%b);//0

		System.out.println(a++);//temp10 but 11 
		System.out.println(b--);//temp2 but 1
		System.out.println(a);//11
		System.out.println(++a);//12
		System.out.println(--b);//0

		int c= a;
		System.out.println(c);//12
		System.out.println(a+=2);//14
		System.out.println(b-=3);//-3 0b1111 1101
		System.out.println(c*=5);//60
		System.out.println(a/=2);//7 0b0000 0111
		
		boolean d=true;
		boolean e=false;
		System.out.println(d&&e);//false
		System.out.println(d||e);//true
		System.out.println(!d);//false
		System.out.println(!e);//true

		System.out.println(a<b);//false
		System.out.println(a>b);//true
		System.out.println(a==b);//false
		System.out.println(a!=b);//true
		System.out.println(a<=b);//false
		System.out.println(a>=b);//true

		System.out.println(a&b);//0b0000 0101 5
		System.out.println(a|b);//0b1111 1111 -1
		System.out.println(a^b);//0b0000 0101 5
		System.out.println(a<<2);//0b0001 1100 30
		System.out.println(a>>2);//0b0000 0001 1
		System.out.println(a>>>2);//0b0000 0001 1
		
}
}
