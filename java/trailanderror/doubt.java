import java.util.*;
class charpattern{
	public static void main(String [] args){
		Scanner sc =new Scanner(System.in);
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=i+64;
			for(int j=1;j<=row+1-i;j++){
				int sum=i+j;
				if(sum%2==0){
					System.out.print((char)num);
				}
				else{
					System.out.print(num);
				}
				num++;
			}
			System.out.println();

		}
	}
}
