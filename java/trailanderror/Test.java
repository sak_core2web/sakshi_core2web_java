class Bicycle{
	public int speed=80;
        public int gear=4;
	public Bicycle(int gear,int speed){
		this.gear=gear;
		this.speed=speed;
	}
	public void applyBrake(int decrement){
		speed-=decrement;
	}
	public void speedUp(int increment){
		speed+=increment;
	}
	public String printData(){
		return("gear is "+gear+"\nspeed is "+speed);
	}
}
class MotorBike extends Bicycle{
	public String model="XYZ";
	public MotorBike(int gear,int speed,String model){
		super(gear,speed);
		this.model=model;
	}
       	public String printData(){
		return(super.printData() + "\nmodel is "+model);
	}	
}
public class Test{
	public static void main(String[] args){
		MotorBike obj= new MotorBike(5,57,"XYZ");
		System.out.println(obj.printData());
	}

}
