class d1{
	public static int multiply(int a,int b){
		return(a*b);
	} 
	public static int multiply(int x,int y,int z){
		return(x*y*z);
	}
	public static float multiply(float p,float q){
		return(p*q);
	}
}
public class demo1{
	public static void main(String [] args){
		d1 d=new d1();
		System.out.println(d.multiply(2,3));
		System.out.println(d.multiply(2,3,10));
		System.out.println(d.multiply(2.2f,10f));
	}
}
